
describe "Matcher#output" do  
    it{expect { puts 'foo'}.to output.to_stdout} 
    it {expect { print 'foo'}.to output('foo').to_stdout } 
    it{expect { p 'foo'}.to output(/foo/).to_stdout} #expressão regular
end 