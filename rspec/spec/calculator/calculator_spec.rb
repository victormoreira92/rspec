require 'calculator'

describe Calculator do
    subject(:calc) { described_class.new() } 
    context "#sum" do
        it "2 numbers" do
            result = calc.sum(5,7)
            expect(result).to eq(12)  
        end

        it "2 numbers negatives" do
            result = calc.sum(-5,-7)
            expect(result).to eq(-12)
        end

        it "1 number negatives" do
            result = calc.sum(-5,7)
            expect(result).to eq(2)
        end
        it "3 numbers or more" do 
            result = calc.sum_with_arr([3,-9,-12,15])
            expect(result).to eq(-3)
        end
    end
    
    context "#multiplication" do
        it "2 numbers" do
         result = calc.multiplication(7,9)
         expect(result).to eq(63)
        end 
        it "3 or more numbers" do
            result = calc.multiplication_with_arr([2,5,3,4])
            expect(result).to eq(120)
        end
    end

    context "#exponentiation" do
        it "2 numbers" do 
          result = calc.exponentiation(2,9)
          expect(result).to eq(512)
        end 
    end
    
end
