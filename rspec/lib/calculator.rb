class Calculator
    def  sum (a,b)
        a+b
    end
    
    def sum_with_arr(list)
        list.sum
    end

    def multiplication(a,b)
        if b <= 1
            return a
        else
          return a + multiplication(a, b-1)
        end
    end

    def multiplication_with_arr(arr)
        arr.inject(:*)
    end

    def exponentiation(a,b)
        a**b
    end

    def exponentiation_with_multiplication(a,b)
        i = 1
        b.times{
         i = multiplication(a,i)
        }
        i
    end
end