class Product < ApplicationRecord
  belongs_to :category
  validates_presence_of :description, :category, :price

  def full_description
    "#{self.description} - #{self.price}"
  end
  
end
