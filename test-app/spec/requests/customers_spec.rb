require 'rails_helper'

RSpec.describe "Customers", type: :request do
  describe "GET /index" do
    customer = Customer.create(name:"Test user")
    get customer_path
    expect(response).to be_successful
    expect(response.body).to include("Test user")
  end
end
