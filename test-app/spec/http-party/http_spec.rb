describe "Http Party" do
    it "http" do
        #stub_request(:get, "https://jsonplaceholder.typicode.com/posts/2").to_return(status: 200, body: "", headers: {'content-type': 'application/json'})
        VCR.use_cassette("application_json") do
            response = HTTParty.get('https://jsonplaceholder.typicode.com/posts/2')
            content_type = response.headers['content-type']
            expect(content_type).to match('/json')
        end
    end
    
end
