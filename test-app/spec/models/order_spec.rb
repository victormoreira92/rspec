require 'rails_helper'

RSpec.describe Order, type: :model do
  it "Tem 1 pedido" do
    order =  create(:order)
    expect(order.customer).to be_kind_of(Customer)
  end

  it"3 pedidos" do
    orders = create_list(:order,3)
    expect(orders).to eq(orders)
  end
  it "has_many" do
    customer = create(:customer, :with_orders)
    puts customer.orders
    expect(customer.orders.count).to eq(4)
  end
end
