require 'rails_helper'

RSpec.describe Customer, type: :model do
  it "Create Customer" do
    @customer = create(:customer, name: "Jackson Pires")
    expect(@customer.name).to eq("Jackson Pires")
  end 
  it 'travel_to' do
    travel_to Time.zone.local(2004, 11, 24, 01, 04, 44) do
      @customer = create(:customer)
    end

    expect(@customer.created_at).to eq(Time.new(2004, 11, 24, 01, 04, 44))
  end
  it "customer_vip" do
    customer = create(:customer_vip)
    expect(customer.vip).to be true
  end 

  it "trait" do
    customer = create(:customer_vip, :male)
    expect(customer.gender).to eq('M')
    expect(customer.vip).to be true

  end 
end
  