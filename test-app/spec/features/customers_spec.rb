require 'rails_helper'
require_relative '../pageObject/newCostumerform'

RSpec.feature "Customers",type: :feature,js: true  do
  it "visit index customer" do
    visit customers_path
    page.save_screenshot('screenshot.png')
    expect(page).to  have_current_path(customers_path)
  end
  it "Ajax" do
    visit(customers_path)  
    click_link('Add Message')
    expect(page).to have_content('Yes!')
  end
  
  it "Find" do
    visit(customers_path)  
    click_link('Add Message')
    expect(find('#my-div').find('h1')).to  have_content('Yes!')
  end
  

  it "create Customer" do
    new_costumer_form = NewCustomerForm.new
    new_costumer_form.visit_page.fill_in_with.submit
    expect(page).to have_content('Customer was successfully created.')
  end
  
end
