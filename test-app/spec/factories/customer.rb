FactoryBot.define do
    factory :customer do
      transient do
        qtt_orders {4}
      end
      name {Faker::Name.name}
      email  {Faker::Internet.email}
      vip {true}
      days_to_pay {30}
    end

    trait :male do
        gender {'M'}
    end
    trait :female do
        gender {'F'}
    end

    trait "with_orders" do
      after(:create) do |customer, evaluator|
        create_list(:order, evaluator.qtt_orders, customer: customer)
      end
    end

    factory :customer_vip, :class=> Customer do
        vip {true}
        days_to_pay {30}
    end

    factory :customer_default, :class=> Customer do
      vip {false}
      days_to_pay {10}
    end
end